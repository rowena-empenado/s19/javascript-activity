console.log("Session 19 - ES6 Updates");

let number = 3;
const getCube = number ** 3;
console.log(`The cube of ${number} is ${getCube}.`)


const address = ["#45", "Jade St.", "Alabang Village", "Muntinlupa City"]

const [houseNumber, street, village, city] = address

console.log(`I live at ${houseNumber} ${street}, ${village}, ${city}.`)


const animal = {
	name: "dolphins",
	habitat: "aquatic",
	size: "6 feet to over 12 feet",
	color: "light gray"
}

const {name, color , habitat, size} = animal;

let message = `${name.toUpperCase()} are ${color} colored ${habitat} animals which generally range in size of ${size}.` ;

function getDescription({name, color, habitat, size}) {
	console.log(`${message}`);
}
getDescription(animal);


const numbers = [5,10, 15, 20, 25]
let multiplier = 2;

numbers.forEach( (num) => {
	let newValue = multiplier * num;
	console.log(newValue)
})


class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const myDog = new Dog();

myDog.name = "Almond";
myDog.age = "7 years old";
myDog.breed = "Aspin";
console.log(myDog);

